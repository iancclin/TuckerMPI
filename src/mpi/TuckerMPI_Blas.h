//
// Created by Work on 2019/10/11.
//

#ifndef TUCKER_TUCKERMPI_BLAS_H
#define TUCKER_TUCKERMPI_BLAS_H

extern "C" {
void dcopy_(const int *, const double *, const int *,
            double *, const int *);

void dsyrk_(const char *, const char *, const int *, const int *,
            const double *, const double *, const int *, const double *, double *,
            const int *);

void dgemm_(const char *, const char *, const int *,
            const int *, const int *, const double *, const double *,
            const int *, const double *, const int *, const double *,
            double *, const int *);
};

#include "Tucker_Timer.hpp"
#include "TuckerMPI_Tensor.hpp"
#include "TuckerMPI_Matrix.hpp"

#endif //TUCKER_TUCKERMPI_BLAS_H
