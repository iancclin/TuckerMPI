//
// Created by Work on 2019/10/11.
//

#ifndef TUCKER_TUCKER_BLAS_H
#define TUCKER_TUCKER_BLAS_H

extern "C" {
// Symmetric rank-k update
void dsyrk_(const char *, const char *, const int *,
            const int *, const double *, const double *, const int *,
            const double *, double *, const int *);
// Symmetric eigenvalue solver
void dsyev_(const char *, const char *, const int *,
            double *, const int *, double *, double *, int *, int *);
// Swap two arrays
void dswap_(const int *, double *, const int *,
            double *, const int *);
// Copy from one array to another
void dcopy_(const int *, const double *, const int *,
            double *, const int *);
// Scale an array
void dscal_(const int *, const double *, double *, const int *);

void dgemm_(const char*, const char*, const int*,
            const int*, const int*, const double*, const double*, const int*,
            const double*, const int*, const double*, double*, const int*);
};

#endif //TUCKER_TUCKER_BLAS_H
